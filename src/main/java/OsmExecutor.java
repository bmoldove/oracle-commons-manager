import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.support.JdbcDaoSupport;

import javax.sql.DataSource;
import java.sql.SQLException;

@Import(OsmConfig.class)
public class OsmExecutor{
    // test the db connection using accsoft db connector

    @Autowired
    private static OsmConfig osmConfig;

    @Bean
    public static void main(String[] args) throws SQLException {

        //OsmConfig config = new OsmConfig();
        DataSource ds = osmConfig.dataSource();
        DbConnectionTest db = new DbConnectionTest(ds);
        db.runDbConnectionTest();
    }

}
