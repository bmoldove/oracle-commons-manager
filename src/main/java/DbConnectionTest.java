import org.springframework.jdbc.core.support.JdbcDaoSupport;

import javax.sql.DataSource;

public class DbConnectionTest extends JdbcDaoSupport{
    private final String SET_DBMS_METADATA_PARAMS = "BEGIN " +
            "  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'STORAGE',FALSE); " +
            "  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'TABLESPACE',FALSE); " +
            "  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'SEGMENT_ATTRIBUTES',FALSE); " +
            //No because of the fix for partitions "  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'CONSTRAINTS','TRUE'); " +
            //No because of the fix for partitions "  DBMS_METADATA.SET_TRANSFORM_PARAM(DBMS_METADATA.SESSION_TRANSFORM,'CONSTRAINTS_AS_ALTER','TRUE'); " +
            "END;";

    public DbConnectionTest(DataSource dataSource){
        setDataSource(dataSource);
    }
    public void runDbConnectionTest(){
        getJdbcTemplate().update(SET_DBMS_METADATA_PARAMS);
    }
}
