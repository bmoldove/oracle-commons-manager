import cern.accsoft.commons.dbaccess.jdbc.DataSourceManager;
import cern.accsoft.commons.dbaccess.jdbc.DataSourceProperties;
import cern.accsoft.commons.dbaccess.jdbc.DefaultDataSourceProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ResourceLoader;

import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@PropertySource("dev.properties")
public class OsmConfig {
    @Autowired
    ResourceLoader resourceLoader;

    @Bean
    public DataSource dataSource() throws SQLException{
        DBProperties c = dbProperties();
        DataSourceManager manager = new DataSourceManager(this.resourceLoader.getResource(c.getTsnamesOraPath()));
        DataSourceProperties properties = dsProperties();
        return manager.getDataSourceFor(DataSourceManager.DataSourceIdentifier.valueOf(c.getSchema()), properties);
    }

    @Bean
    @ConfigurationProperties(prefix= "spring.datasource.data-source-properties")
    public DataSourceProperties dsProperties(){return new DefaultDataSourceProperties();}

    @Bean
    @ConfigurationProperties(prefix = "spring.datasource.db-properties")
    public DBProperties dbProperties(){return new DBProperties();}

    static class DBProperties{
        private String tnsnamesOraPath;
        private String schema;

        public String getTsnamesOraPath(){
            return tnsnamesOraPath;
        }
        public void setTsnamesOraPath(String tsnamesOraPath){
            this.tnsnamesOraPath = tsnamesOraPath;
        }

        public String getSchema(){
            return schema;
        }
        public void setSchema(String schema){
            this.schema = schema;
        }
    }
}
